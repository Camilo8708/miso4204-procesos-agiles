# cc.huertas    -   Fibonacci

# Solicitud de Iteraciones
ciclo = raw_input("Ingrese el numero de Iteraciones  ")
while ciclo == "" or not ciclo.isdigit():
    ciclo = raw_input("Ingrese el numero de Iteraciones  ")

# Definición de Variables
cnt = 0
fibo = 0
fibo1 = 0
fibo2 = 1

# Ciclo de calculo de Fibonacci
while cnt <= int(ciclo):
    if cnt == 0:
        resp = str(0)

    elif cnt == 1:
        resp = resp + ", " + str(1)

    else:
        fibo = fibo1 + fibo2;
        resp = resp + ", " + str(fibo)
        fibo1 = fibo2
        fibo2 = fibo

    cnt += 1

# Impresión Resultado
print resp
